<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::hidden('garage_id', request('id'), null) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Chain:</strong>
            {!! Form::text('chain', null, array('placeholder' => 'Chain','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Front Sprocket:</strong>
            {!! Form::text('front_sprocket', null, array('placeholder' => 'Front Sprocket','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Rear Sprocket</strong>
            {!! Form::text('rear_sprocket', null, array('placeholder' => 'Rear Sprocket','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Number of Links</strong>
            {!! Form::text('links', null, array('placeholder' => 'Number of Links','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Chain Slack</strong>
            {!! Form::text('slack', null, array('placeholder' => 'Chain Slack','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>