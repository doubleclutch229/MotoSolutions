<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2><strong>Drive</strong></h2>
        </div>

        <div style="float: right">
            <a class="btn btn-primary" href="{{ route('drive.edit',$garage->id) }}">Edit</a>

            {!! Form::open(['method' => 'DELETE','route' => ['drive.destroy', $garage->id],'style'=>'float:right']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>

    </div>
</div>

@foreach ($garage->drive as $drive)

<table class="table table-bordered">
    <tr>
        <th>Chain</th>
        <th>Front Sprocket</th>
        <th>Rear Sprocket</th>
        <th>Links</th>
        <th>Slack</th>
    </tr>

    <tr>
        <td>{{ $drive->chain}}</td>
        <td>{{ $drive->front_sprocket}}</td>
        <td>{{ $drive->rear_sprocket}}</td>
        <td>{{ $drive->links}}</td>
        <td>{{ $drive->slack}}</td>
    </tr>
</table>

@endforeach
