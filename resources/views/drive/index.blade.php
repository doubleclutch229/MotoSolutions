<h2>Drivetrain</h2>

<a class="btn btn-success" href="{{ route('drive.create', [$garage->id]) }}"> Add Drive</a>

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

<table class="table table-bordered">
    <tr>
        <th>Chain</th>
        <th>Front Sprocket</th>
        <th>Rear Sprocket</th>
        <th>Links</th>
        <th>Slack</th>
    </tr>

          <tr>
              <td>{{ $garage->drive}}</td>
              <td>{{ $garage->front_sprocket}}</td>
              <td>{{ $garage->rear_sprocket}}</td>
              <td>{{ $garage->links}}</td>
              <td>{{ $garage->slack}}</td>
              <td>
                  <a class="btn btn-primary" href="{{ route('drive.edit',$garage->id) }}">Edit</a>
                  {!! Form::open(['method' => 'DELETE','route' => ['drive.destroy', $garage->id],'style'=>'display:inline']) !!}
                  {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                  {!! Form::close() !!}
              </td>
          </tr>

</table>



