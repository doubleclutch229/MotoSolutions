<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::hidden('garage_id', request('id'), null) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Fork Compression:</strong>
            {!! Form::text('fork_compression', null, array('placeholder' => 'Fork Compression','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Fork Rebound:</strong>
            {!! Form::text('fork_rebound', null, array('placeholder' => 'Fork Rebound','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Fork Height:</strong>
            {!! Form::text('fork_height', null, array('placeholder' => 'Fork Height','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Fork Oil Level:</strong>
            {!! Form::text('fork_oil_level', null, array('placeholder' => 'Fork Oil Level','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Fork Spring:</strong>
            {!! Form::text('fork_spring', null, array('placeholder' => 'Fork Spring','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Shock Compression:</strong>
            {!! Form::text('shock_compression', null, array('placeholder' => 'Shock Compression','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Shock Rebound:</strong>
            {!! Form::text('shock_rebound', null, array('placeholder' => 'Shock Rebound','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Shock Spring:</strong>
            {!! Form::text('shock_spring', null, array('placeholder' => 'Shock Spring','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Shock Oil Level:</strong>
            {!! Form::text('shock_oil_level', null, array('placeholder' => 'Shock Oil Level','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Shock Nitorgen:</strong>
            {!! Form::text('shock_nitrogen', null, array('placeholder' => 'Nitrogen','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>