<h2>Suspension</h2>

<a class="btn btn-success" href="{{ route('suspension.create') }}"> Add Suspension</a>



@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

<table class="table table-bordered">
    <tr>
        <th>Fork Compression</th>
        <th>Fork Rebound</th>
        <th>Fork Height</th>
        <th>Fork Oil Level</th>
        <th>Fork Spring</th>

    </tr>

</table>

<table class="table table-bordered">
    <tr>
        <th>Shock Compression</th>
        <th>Shock Rebound</th>
        <th>Shock Preload</th>
        <th>Shock Spring</th>
        <th>Shock Oil Level</th>
        <th>Shock Nitrogen Amount</th>
    </tr>
</table>

