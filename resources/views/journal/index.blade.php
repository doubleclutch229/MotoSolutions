@extends('layout')

<h2>Ride Journal</h2>

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif


<table class="table table-bordered">
    <tr>
        <th>Date</th>
        <th>Location</th>
        <th>Seat Time</th>
        <th>Notes</th>
    </tr>




   <tr>
        <td>{{ $garages->journal->date}}</td>
        <td>{{ $journal->location}}</td>
        <td>{{ $journal->seat_time}}</td>
        <td>{{ $journal->notes}}</td>
    </tr>


</table>


