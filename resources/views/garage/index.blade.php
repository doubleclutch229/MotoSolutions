@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>MotoSolutions Garage</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('garage.create') }}"> Add a vehicle to the Garage</a>
            </div>
        </div>
        </div>



    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>Year</th>
            <th>Make</th>
            <th>Model</th>
        </tr>


       @foreach ($garage as $vehicle)
            <tr>
                <td>{{ $vehicle->year}}</td>
                <td>{{ $vehicle->make}}</td>
                <td>{{ $vehicle->model}}</td>
                <td>
                    <div class="pull-right">
                    <a class="btn btn-info" href="{{ route('garage.show',$vehicle->id) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('garage.edit',$vehicle->id) }}">Edit</a>
                    {!! Form::open(['method' => 'DELETE','route' => ['garage.destroy', $vehicle->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                    </div>
                </td>
            </tr>
        @endforeach

    </table>
    </div>






@endsection