


<div class="row">
    <div class="form-group">
        {!! Form::hidden('user_id', $id, null) !!}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Year</strong>
            {!! Form::text('year', null, array('placeholder' => 'Year','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Make</strong>
            {!! Form::text('make', null, array('placeholder' => 'Make','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Model</strong>
            {!! Form::text('model', null, array('placeholder' => 'Make','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="row cancel">
        <div class="col-md-4">
            {!! Form::file('image', array('class' => 'image')) !!}
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>

</div>

