<h2>Carburetor</h2>
<a class="btn btn-success" href="{{ route('carbjetting.create') }}"> Add a Carb Setting </a>


@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif


<table class="table table-bordered">
    <tr>
        <th>Pilot</th>
        <th>Main</th>
        <th>Clip Position</th>
        <th>Float Height</th>
        <th>Idle Screw</th>
        <th>Air Fuel Screw</th>
    </tr>

    {{--  <tr>
          <td>{{ $carbjetting->pilot}}</td>
          <td>{{ $carbjetting->main}}</td>
          <td>{{ $carbjetting->clip_pos}}</td>
          <td>{{ $carbjetting->float_height}}</td>
          <td>{{ $carbjetting->idle_screw}}</td>
          <td>{{ $carbjetting->airfuel_screw}}</td>
          <td>
              <a class="btn btn-info" href="{{ route('carbjetting.show',$vehicle->id) }}">Show</a>
              <a class="btn btn-primary" href="{{ route('carbjetting.edit',$vehicle->id) }}">Edit</a>
              {!! Form::open(['method' => 'DELETE','route' => ['carbjetting.destroy', $vehicle->id],'style'=>'display:inline']) !!}
              {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
              {!! Form::close() !!}
          </td>
      </tr>--}}
</table>



