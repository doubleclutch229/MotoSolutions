<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GarageController@index');

Route::resource('garage','GarageController');

Route::resource('carbjetting','CarbController');

Route::resource('drive','DriveController');

Route::resource('journal','JournalController');

Route::resource('suspension','SuspensionController');

Auth::routes();

/*Route::get('/home', [
    'middleware' => ['auth'],
    'uses' => function () {
        echo "You are allowed to view this page!";
    }]);*/
