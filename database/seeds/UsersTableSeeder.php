<?php

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();
        User::create(array(
            'name'     => 'jon crissey',
            'email'    => 'joncrissey@gmail.com',
            'password' => Hash::make('password'),
        ));
    }

}