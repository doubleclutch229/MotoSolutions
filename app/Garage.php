<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Garage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','user_id','year', 'make', 'model'
    ];

    public function journals()
    {
        return $this->hasMany('App\Journal');
    }

    /**
     * Get the User record associated with the Garage item.
     */
    public function users()
    {
        return $this->belongsTo('App\User');
    }


    /**
     * Get the carbjetting record associated with the garage item.
     */
    public function carbjettings()
    {
        return $this->hasMany('App\carbJetting');
    }


    /**
     * Get the drive record associated with the garage item.
     */
    public function drives()
    {
        return $this->hasMany('App\Drive');
    }

    /**
     * Get the Suspension record associated with the garage item.
     */
    public function suspensions()
    {
        return $this->hasMany('App\Suspension');
    }


}
