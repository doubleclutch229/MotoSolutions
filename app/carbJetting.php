<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carbJetting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'garage_id', 'pilot', 'main', 'needle', 'clip_pos', 'float_height', 'idle_screw', 'airfuel_screw'
    ];


    /**
     * Get the garage record associated with the drive item.
     */
    public function garage()
    {
        return $this->belongsTo('App\Garage', 'garage_id');
    }

}
