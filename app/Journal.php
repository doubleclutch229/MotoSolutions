<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'garage_id','date','location', 'seat_time', 'notes'
    ];


    /**
     * Get the garage record associated with the drive item.
     */
    public function garage()
    {
        return $this->belongsTo('App\Garage', 'garage_id');
    }
}
