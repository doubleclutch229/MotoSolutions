<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suspension extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'garage_id','fork_compression','fork_rebound','fork_height','fork_oil_level','fork_spring',
        'shock_compression', 'shock_rebound', 'shock_spring', 'shock_oil_level', 'shock_nitrogen'
    ];

    protected $hidden = ['id'];


    /**
     * Get the garage record associated with the drive item.
     */
    public function garage()
    {
        return $this->belongsTo('App\Garage', 'garage_id');
    }

    public function delete(){

    }

}
