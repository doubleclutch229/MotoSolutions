<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drive extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'garage_id', 'chain', 'front_sprocket', 'rear_sprocket', 'links', 'slack'
    ];


    /**
     * Get the garage record associated with the drive item.
     */
    public function garage()
    {
        return $this->belongsTo('App\Garage', 'garage_id');
    }

}
